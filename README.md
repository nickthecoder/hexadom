# hexadom

A strategy game - control a kingdom keeping your enemies and nature at bay.
The game is played on a hexagonal grid.
It falls somewhere in the middle between a real-time strategy game and a turn based game.
The pace isn't fast, but you are free to move whenever you want.

To play the game, you must first install
[Tickle](https://gitlab.com/nickthecoder/tickle).

Start Tickle, then click "Play", and choose the file : "hexadom.tickle".
(If you've set up the proper file associations for ".tickle" files, you can double click it instead).



Powered by [Tickle](https://gitlab.com/nickthecoder/tickle) and [LWJGL](https://www.lwjgl.org/).
